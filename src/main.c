#define _DEFAULT_SOURCE

#include "mem.h"
#include "mem_internals.h"

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/mman.h>
#include <unistd.h>


struct block_header* header_from_data(void* data){
	return (struct block_header*)(data - offsetof(struct block_header, contents));
}

void test_alloc_free();
void test_region_extend();
void test_region_intersection();

int main() {

	test_alloc_free();
	test_region_extend();
	test_region_intersection();

	printf("All tests passed!\n");

	return 0;
}


void test_region_intersection(){
	printf("Running region intersection test!\n");
	size_t size = getpagesize() * 2 - offsetof(struct block_header, contents);
	heap_init(size);

	void* mem1 = _malloc(size);

	int* res = mmap((void*) HEAP_START + getpagesize() * 2, getpagesize() * 2, PROT_READ | PROT_WRITE,
			MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);
	assert(res != MAP_FAILED);

	void* mem2 = _malloc(REGION_MIN_SIZE);
	void* mem3 = _malloc(REGION_MIN_SIZE / 2);

	assert(mem1 != NULL);
	assert(mem2 != NULL);
	assert(mem3 != NULL);

	assert(header_from_data(mem1)->capacity.bytes == size);
	assert(header_from_data(mem2)->capacity.bytes == REGION_MIN_SIZE);
	assert(header_from_data(mem3)->capacity.bytes == REGION_MIN_SIZE / 2);

	assert(header_from_data(mem1)->next == header_from_data(mem2));
	assert(mem1 + header_from_data(mem1)->capacity.bytes != mem2);

	_free(mem1);
	assert(header_from_data(mem1)->is_free);

	_free(mem2);
	assert(header_from_data(mem2)->is_free);

	_free(mem3);
	assert(header_from_data(mem3)->is_free);

	munmap((void*) HEAP_START + getpagesize() * 2, getpagesize() * 2);

	heap_term();
}

void test_region_extend(){
	printf("Running region extending test!\n");
	heap_init(REGION_MIN_SIZE);
	void* mem1 = _malloc(REGION_MIN_SIZE / 2);
	void* mem2 = _malloc(REGION_MIN_SIZE);
	void* mem3 = _malloc(REGION_MIN_SIZE / 2);

	assert(mem1 != NULL);
	assert(mem2 != NULL);
	assert(mem3 != NULL);

	assert(header_from_data(mem1)->capacity.bytes == REGION_MIN_SIZE / 2);
	assert(header_from_data(mem2)->capacity.bytes == REGION_MIN_SIZE);
	assert(header_from_data(mem3)->capacity.bytes == REGION_MIN_SIZE / 2);

	assert(header_from_data(mem1)->next == header_from_data(mem2));
	assert(header_from_data(mem2)->next == header_from_data(mem3));

	assert(mem1 + header_from_data(mem1)->capacity.bytes == header_from_data(mem2));
	assert(mem2 + header_from_data(mem2)->capacity.bytes == header_from_data(mem3));

	_free(mem1);
	assert(header_from_data(mem1)->is_free);

	_free(mem2);
	assert(header_from_data(mem2)->is_free);

	_free(mem3);
	assert(header_from_data(mem3)->is_free);

	heap_term();
}

void test_alloc_free(){
	printf("Running default allocation tests!\n");
	heap_init(REGION_MIN_SIZE);
	void* mem1 = _malloc(16);
	void* mem2 = _malloc(2048);
	void* mem3 = _malloc(8192);

	assert(mem1 != NULL);
	assert(mem2 != NULL);
	assert(mem3 != NULL);

	assert(header_from_data(mem1)->capacity.bytes >= 16);
	assert(header_from_data(mem2)->capacity.bytes == 2048);
	assert(header_from_data(mem3)->capacity.bytes == 8192);

	_free(mem1);
	assert(header_from_data(mem1)->is_free);

	_free(mem2);
	assert(header_from_data(mem2)->is_free);

	_free(mem3);
	assert(header_from_data(mem3)->is_free);

	heap_term();
}
